//AUTHOR:Chris Saam
//FILE: ThreeNodeShell
//CLASS PROVIDED: ThreeNodeShell (a class abstracted from 
//ElementClass containind three addresses of Point3D objects
//contained in a user provided array.)
//CONSTRUCTORS for the ThreeNodeShell class:
//	ThreeNodeShell(int inSize, int* inNodes)
//	Precondition: inSize==3
//	Postcondition: the ThreeNodeShell object contains
//		the array addresses stored in inNodes.)
//CONSTANT MEMBER FUNCTIONS for the ThreeNodeShell class:
//	int numNodes()
//	Postcondition: the integer value 3 is returned.
//	int getNode(int position)
//	Postcondition: the array address stored at position is returned.
//	void match(ResizeArray<Point3D*>& allPoints,
//	const Point3D& matchPoint,const double tol, MatchInfo& info)const
//	Postcondition: an attempt is made to match matchPoint to
//		a point within the ThreeNodeShell and a struct of type
//		MatchInfo is returned containing information about this 
//		attempt.  (see ElementClass.h)
#ifndef _THREE_NODE_SHELL
#define _THREE_NODE_SHELL
#include<cassert>
#include<cmath>
#include"ElementClass.h"
#include"Point3D.h"
#include"tools.h"
#include<cstdlib>
class ThreeNodeShell:public ElementClass{
private:
  int nodes[3];
public:
	ThreeNodeShell(int inSize, int* inNodes);
	int numNodes() { return 3; }
	int getNode(int position);
	void match(ResizeArray<Point3D*>& allPoints,
		const Point3D& matchPoint,const double tol, MatchInfo& info)const; 
        void getLocalCoordinates(int i, double &xi, double &eta);
};
#endif
