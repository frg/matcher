#ifndef _QUADRALATERAL_
#define _QUADRALATERAL_
#include<cassert>
#include<cmath>
#include"ElementClass.h"
#include"Point3D.h"
#include"tools.h"
class Quadralateral3D:public ElementClass{
private:
	int nodes[4];
public:
	Quadralateral3D(int inSize, int* inNodes);
	int numNodes() {return 4;}
	int getNode(int position);
	void match(ResizeArray<Point3D*>& allPoints,
                   const Point3D& matchPoint, const double tol, MatchInfo& info) const;
        void getLocalCoordinates(int i, double &xi, double &eta);
};
#endif
