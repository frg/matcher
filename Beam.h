#ifndef _BEAM_
#define _BEAM_

#include<cassert>
#include<cmath>
#include<cstdlib>
#include"ElementClass.h"
#include"Point3D.h"
#include"tools.h"

class Beam:public ElementClass{
private:
	int nodes[2];
	
public:
	Beam(int inSize, int* inNodes);
	int numNodes(){return 2;}
        int getNode(int position);
	void match(ResizeArray<Point3D*>& allPoints,
                   const Point3D& matchPoint,const double tol, MatchInfo& info)const;
        void getLocalCoordinates(int i, double &xi, double &eta);
};
#endif
