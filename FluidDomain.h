#ifndef _FLUIDDOMAIN_
#define _FLUIDDOMAIN_

#include <iomanip>
#include <cassert>
#include <cmath>
#include <iostream>
using std::ifstream;
using std::ofstream;
using std::ios;
using std::cerr;
using std::endl;
#include <cstring>
#include <fstream>
#include <cstdlib>

#include "Point3D.h"
#include "Elem.h"
#include "Face.h"

extern int dimension;

#define MIN_TYPE_BC -6
#define MAX_TYPE_BC 10

//--------------------------------------------------------------------

class NodeSet {

  int numNodes;

  Point3D *nodes;  
  int     *type_bc;
  int      size_max;

public:

  NodeSet() {
    numNodes = 0;
    size_max  = 0;
    nodes = NULL;
    type_bc = NULL;
  }

  ~NodeSet() {
    numNodes = 0;
    size_max     = 0;
    delete [] nodes;
    delete [] type_bc;
  }

  Point3D &operator[](int i) { return nodes[i]; }

  int size() {return numNodes;}

  void addNode(int i, Point3D node) { 
    nodes[i] = node; 
  }  

  int resize(int nn) {
    
    if (nn>size_max) {
      // Only reallocate memory for resized array 
      // if new number of nodes is larger 
      // than current size_max
      Point3D *nodes_new = new Point3D[nn];
    
      // Copy old array to new one
      for (int i=0; i<size_max; i++)
	nodes_new[i] = nodes[i];

      // Delete old array
      delete [] nodes;

      // Set fields to new values
      nodes = nodes_new;
      size_max  = nn;

      // If type_bc was allocated resize it too
      if (type_bc) {
	int *type_bc_new = new int[nn];
	
	// Copy old array to new one
	for (int i=0; i<size_max; i++)
	  type_bc_new[i] = type_bc[i];
	
	// Delete old array
	delete [] type_bc;	

	// Set fields to new values
	type_bc = type_bc_new;
      }
    }
    
    // New number of nodes
    return numNodes = nn;
  }

  void setTypeBC(int i, int t) {
    if (!type_bc)
      type_bc = new int[size_max];      

    type_bc[i] = t;
  }

  int getTypeBC(int i) {
    return type_bc[i];
  }

};

//--------------------------------------------------------------------

class FluidDomain {
    
public:

  NodeSet nodes;
  ElemSet elems;
  FaceSet faces;

  virtual void getGeometry(char* file) = 0;
  virtual void setTypeBC(int i, int t) = 0;
  virtual int  getTypeBC(int i) = 0;

};

//--------------------------------------------------------------------

class FluidDomain3D : public FluidDomain {

  bool*  type_active;
  char** type_name;
    
  void getFluidDomainXpost(char* meshname);
  void getFluidDomainFieldView(char* meshname);
  void getFluidDomainSinus(char* meshname);

public:

  FluidDomain3D();
  ~FluidDomain3D();

  void getGeometry(char* file);
  void setTypeBC(int i, int t) { faces[i].setTypeBC(t); }
  int  getTypeBC(int i) { return faces[i].getTypeBC(); }

};

//--------------------------------------------------------------------

class FluidDomain2D : public FluidDomain {

public:

  FluidDomain2D() {}
  ~FluidDomain2D() {}

  void getGeometry(char* file);

  void setTypeBC(int i, int t) { nodes.setTypeBC(i, t); }
  int  getTypeBC(int i) { return nodes.getTypeBC(i); }

};

//--------------------------------------------------------------------

#endif
