#ifndef _DECOMP2D_H_
#define _DECOMP2D_H_
//AUTHOR: Chris Saam
//FILE:	Decomp2D.h
//CLASS PROVIDED: Decomp2D (this class is abstracted from Decomp class
//and contains all the necessary data for the fluid domian and it's
//decomposed subdomains)
//PUBLIC MODIFICATION MEMBER FUNCTIONS for Decomp2D class:
//void getFluidDomain(char* name)
//Preconditions: name is a character pinter pointing a char array
//	containing the name of a valid fluid mesh file
//Postconditions: the file has been read and the fluidDom struct
//	has been filled with the number of points and facets as well
//	as lists of said objetcs stored in dynamic arrays
//void getDecomp(char* name)
//Precondition: name points to a char array contating the name of
//	a valid structural domain file
//Postconditions: the file has been read and the subToTri Connectivity
//	is set
//void getDomainConnect()
//Preconditions: all connectivties have been set except interfToNode
//Postconditions: interfToNode has been set (the interfaces have been found)
//void makeSubD(int noOverlap)
//Precondition: the fluid mesh and decomposition have been read in 
//Postycondition: all connectivities except interfToNode have been ser
//int getNumSub()
//Precondition: the fluid mesh and decomp files have been read
//Postcondition: returns the number of subdomains in this decomposition
//Connectivity* getSubToNode()
//Precondition: the subToNode connectivity has been set
//Postcondition: returns the subToNode connectivity
//PRIVATE MODIFICATION MEMBER FUNCTIONS for class Decomp2D
//Connectivity* smoothDown(int smoothNum, Connectivity* triToTri)
//Precondition: subToTri has been constructed
//Postcondition: updates subToTri by creating smoothNum layers of overlap
//	and then gives the each element in the overlap to the lowest numbered
//	subdomain which posses it 

//Connectivity* smoothUp(int smoothNum, Connectivity* triToTri)
//Precondition: subToTri has been constructed
//Postcondition: updates subToTri by creating smoothNum layers of overlap
//  and then gives the each element in the overlap to the highest numbered
//  subdomain which posses it 
//Note: smoothUp and smoothDown are used together in order to remove any
//	segement of a subdomain which is too small (ie thichness less then 3
//	elements
//CONSTANT MEMEBER FUNCTIONS for Decomp
//FluidDomain* getDomain()
//Precondition: the fluid mesh has been read
//Postcondition:returns the fluidDom struct 
//void print();
//Precondition: all connectivities have been set
//Postcondition: prints the flu-*, match.dec, and flu.glob files


#include<cmath>
#include<iostream>
using std::ifstream;
using std::ofstream;
using std::ios;
using std::ios_base;
using std::cerr;
using std::endl;
#include<cstring>
#include<fstream>
#include<cstdlib>
#include"Connectivity.h"
#include "Point3D.h"
#include "Face.h"
#include "aux.h"
#include "Decomp.h"
#include "tools.h"

class Decomp2D : public Decomp{
private:
  int numSub;

  // Pointer to an integer array of size numNode
  // this array contains the number of the master
  // subdomain of the point havind the same address in
  // the node array conatained in fluiDom
  int* master; 

  // Pointer an array conataining the number of
  // of interface nodes in each subdomain
  int* nConnect; 

  // This pointer to as list of pointers
  // is used to construct a 2D array
  // containing the subdomains that touch
  // a particular subdomain.  Both nConnect
  // and connectedDoamin are necessary to
  // maintain the ordering used in interfNode
  int** connectedDomain; 
  
  //connectivity of subs to elements
  Connectivity* subToTri; 

  //connectivity of subs to nodes
  Connectivity* subToNode;
  
  //inverse connectivity of subToNode
  Connectivity* nodeToSub;

  //connectivity of subs to subs
  Connectivity* subToSub;
  
  //connectivity of ineterfaces to nodes
  Connectivity** interfNode;
  
  Connectivity* smoothDown(int smoothNum, Connectivity* triToTri);
  Connectivity* smoothUp(int smoothNum, Connectivity* triToTri);
  
public:

  Decomp2D();
  ~Decomp2D();

  void getFluidDomain(char* name);
  void getDecomp(char* name);
  void getDomainConnect();
  void makeSubD(int noOverlap);
  int getNumSub(){return numSub;}

  Connectivity* getSubToNode(){return subToNode;}
  FluidDomain* getDomain(){return fluidDom;}
  void print();
};

#endif
