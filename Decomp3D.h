//AUTHOR: Chris Saam
//FILE: Decomp3D.h
//CLASS PROVIDED Decomp3D (this class contains all necessary 
//information for the fluid domain and it's decomposed subdomains)
//MODIFICATION MEMBER FUNCTIONS for Decomp3D class:
//void getFluidDomain(char* name);
//Preconditions: name is a pointer to a char array containing
//	a the name of a valid fluid structure file
//Postcondition: the structure pointed to by fluidDom is
//	filled with the number of points, elements, and facets 
//	as well as lists of said objects stored in dynamic arrays.
//	Also the boundary conditions for the facets is stored.
//void getDecomp(char* name);
//Precondition: name is a pointer to a char array containing
//	the name of a valid fluid domain decomposition file
//Postcondtion: The decomposition file is read and the subToTet
//	is set.
//void getDomainConnect(int noOverlap);
//Postcondition: nConnect and connectedDomain are created and 
//	interfNode is set.  noOverlap has no affaect on Decomp3D
//	but is implemented only to make Decomp3D have the same
//	form as Decomp2D 
//void makeSubD();
//Preconditions: all connectivties have been set except interfToNode
//Postcondition: all other connectivities 
//	are set.
//CONSTANT MEMBER FUNCTIONS for Decomp3D
//Connectivity* getSubToNode(){return subToNode;}
//Postcondition: A pointer to the subToNode connectivity is returned
//FluidDomain* getDomain(){return fluidDom;}
//Postcondition: A pointer to the fluidDom struct is returned.
//void print();
//Postcondition: The flu-* and flu.glob files are outputted.


#ifndef _DECOMP3D_H_
#define _DECOMP3D_H_
#include <iomanip>
#include <cassert>
#include <cmath>
#include <iostream>
using std::ifstream;
using std::ofstream;
using std::ios;
using std::cerr;
using std::endl;
#include <cstring>
#include <fstream>
#include <cstdlib>
#include "Connectivity.h"
#include "Point3D.h"
#include "Elem.h"
#include "Face.h"
#include "aux.h"
#include "Decomp.h"

class Decomp3D:public Decomp{
private:
  int numSub;

  // Pointer an array conataining the number of
  // of interface nodes in each subdomain
  int* nConnect; 

  // This pointer to as list of pointers
  // is used to construct a 2D array 
  // containing the subdomains that touch
  // a particular subdomain.  Both nConnect
  // and connectedDoamin are necessary to
  // maintain the ordering used in interfNode
  int** connectedDomain; 

  // Connectivity of subs to elements
  Connectivity*  subToElem;

  // Connectivity of subs to nodes
  Connectivity*  subToNode;

  // Connectivity of subs to facets
  Connectivity*  subToFace;

  // Inverse connectivity of subToNode
  Connectivity*  nodeToSub;

  // Connectivity of subs to subs
  Connectivity*  subToSub;
  
  // Connectivity of ineterfaces to nodes
  Connectivity** interfNode;

public:

  Decomp3D();
  ~Decomp3D();

  void getFluidDomain(char* name);

  void getDecomp(char* name);
  void getDomainConnect();

  void makeSubD(int noOverlap);

  int getNumSub(){return numSub;}

  Connectivity* getSubToNode(){return subToNode;}
  FluidDomain* getDomain(){return fluidDom;}
  void print();

};
#endif
