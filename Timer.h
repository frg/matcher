#ifndef _TIMER_H_
#define _TIMER_H_

class Communicator;

//------------------------------------------------------------------------------

class Timer {
/*
  enum TimerIndex {
    setup, run, total,
    fluid, nodalWeights, nodalGrad, advFluxes, h1Assembly, h2Assembly, 
    fluidPrecSetup, fluidKsp, meshMetrics, 
    mesh, meshAssembly, meshPrecSetup, meshKsp,
    comm, localCom, globalCom, interCom,
  };

  int numTimings;

  //double initialTime;

  int *counter;
  double *data;

*/
public:

  Timer();
  ~Timer();

  double getTime();
  //void print(Timer *, FILE * = stderr);

};

//------------------------------------------------------------------------------

#endif
