#ifndef	_ELEM_H_
#define	_ELEM_H_
#include<cstdlib>
#include<cassert>
#include<iomanip>
#include<iostream>
using std::ostream;
using std::istream;

#include "BlockAlloc.h"

//--------------------------------------------------------------------

class Elem {
public:
  enum Type {TET=5, PRI=10};
  static const int MaxNumNd = 8;

protected:

  virtual int* nodeNum() = 0;
  virtual int& nodeNum(int i) = 0;

public:

  virtual int getType() = 0;

  // Number of nodes
  virtual int numNodes() = 0;

  // Operator Elem[i]: get reference to node i
  int &operator[](int i) { return nodeNum(i); }  

  // Operator *(Elem): get pointer to node list
  operator int *() { return nodeNum(); }

  // Fills the nd array with the list of nodes
  // and returns pointer to list of nodes
  int* nodes(int* d = NULL) {  

    if (d)
      for (int i=0; i<numNodes(); i++)
	d[i] = nodeNum(i);

    return nodeNum();
  }

  friend ostream& operator<<(ostream& os, Elem& source) {
    int i;
    for (i=0; i<source.numNodes()-1; i++)
      os<<source[i]+1<<' ';
    return os<<source[i]+1;
  }

  friend istream& 
  operator>>(istream& is, Elem& source){
    for (int i=0; i<source.numNodes(); i++) {
      is>>source[i];
      source[i]--;
    }
    return is;
  }
  
};

//--------------------------------------------------------------------

class ElemTet : public Elem {
private:
  int node[4];

protected:
  int* nodeNum() { return node; }
  int& nodeNum(int i) { return node[i]; }

public:
  int numNodes() { return 4; }
  int getType()  { return TET; }

  ElemTet() {    
    for (int i=0; i<numNodes(); i++) 
      nodeNum(i) = -1;
  }

  ElemTet(int *nn) {    
    for (int i=0; i<numNodes(); i++)
      nodeNum(i) = nn[i];
  }

  ElemTet(int _n1, int _n2, int _n3, int _n4) { 
    node[0] = _n1; node[1] = _n2;
    node[2] = _n3; node[3] = _n4;
  }

};

//--------------------------------------------------------------------

class ElemPri : public Elem {
private:
  int node[6];

protected:
  int* nodeNum() { return node; }
  int& nodeNum(int i) { return node[i]; }

public:
  int  numNodes() { return 6; }
  int  getType()  { return PRI; }

  ElemPri() {    
    for (int i=0; i<numNodes(); i++) 
      nodeNum(i) = -1;
  }

  ElemPri(int *nn) {    
    for (int i=0; i<numNodes(); i++)
      nodeNum(i) = nn[i];
  }

  ElemPri(int _n1, int _n2, int _n3, 
	  int _n4, int _n5, int _n6) { 
    node[0] = _n1; node[1] = _n2; node[2] = _n3;
    node[3] = _n4; node[4] = _n5; node[5] = _n6;
  }

};

//--------------------------------------------------------------------

class ElemSet {

  int numElems;

  Elem **elems;
  int    size_max;

  BlockAlloc memElems;
  
public:

  ElemSet() {
    numElems = 0;
    size_max = 0;
    elems = NULL;
  }

  ~ElemSet() {
    numElems = 0;
    size_max  = 0;
    delete [] elems;
  }

  Elem &operator[](int i) { return *elems[i]; }

  int size() {return numElems;}

  void addElem(int i, Elem *elem) { 
    elems[i] = elem; 
  }

  int resize(int ne) {
    
    if (ne>size_max) {
      // Only reallocate memory for resized array 
      // if new number of elems is larger 
      // than current size_max
      Elem **elems_new = new Elem*[ne];
    
      // Copy old array to new one
      for (int i=0; i<size_max; i++)
	elems_new[i] = elems[i];

      // Delete old array
      delete [] elems;

      // Set fields to new values
      elems = elems_new;
      size_max  = ne;
    }
    
    // New number of elems
    return numElems = ne;
  }

  BlockAlloc &getMem() {return memElems;}
  
};

//--------------------------------------------------------------------

#endif
