%{
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "aux.h"
#if defined(linux)
extern int  yylex(void);
extern void yyerror(const char  *);
#endif
 int numColumns = 3;
 double amplitude = 1.0;
 int PitaTS = 1;         //CD: Pita
%}

%union
{
 int ival;
 double fval;
 char * strval;
 NumedNode nval;
 NumedElem eval;
 NumList nl;
 DoubleList dlist;
 NodalFrameData nframe;
/*
 BCond bcval;
 BCList *bclist;
 LMPCons *lmpcons;
 MFTTData *ymtt;
 MFTTData *ctett;
 ComplexBCList *cxbclist;
 ComplexBCond cxbcval;
 FrameData frame;
 MFTTData *mftval;
 MFTTData *mptval;
 MFTTData *hftval;
 LayerData ldata;
 LayInfo *linfo;
 CoefData coefdata;
 ComplexFDBC complexFDBC;
 ComplexFNBC complexFNBC;
 AxiMPC axiMPC;
 SurfaceEntity* SurfObj;
 MortarHandler* MortarCondObj;
 LMPCTerm* mpcterm;
*/
}

%token ACTUATORS AERO AEROH AEROTYPE ARCLENGTH ATTRIBUTES 
%token AUGMENT AUGMENTTYPE AVERAGED ATDARB ACOUSTIC ATDDNB ATDROB
%token AXIHDIR AXIHNEU AXINUMMODES AXINUMSLICES AXIHSOMMER AXIMPC
%token BOFFSET BUCKLE BGTL
%token COARSESOLVER COEF CFRAMES COLLOCATEDTYPE CONSTRMAT CONVECTION COMPOSITE CONDITION 
%token CONTROL CORNER CORNERTYPE CURVE CCTTOL CCTSOLVER COMPLEXOUTTYPE CRHS
%token DAMPING DblConstant DIMASS DISP DIRECT DLAMBDA DOFTYPE DP DYNAM DUALTOL DETER DECOMPOSE
%token EIGEN EFRAMES ELSCATTERER END EPROJ ELHSOMMERFELD EXPLICIT
%token FACOUSTICS FETI FETI2TYPE FETIPREC FFP FITALG FNAME FLUX FORCE FRONTAL FPROJ FETIH
%token FREQSWEEP FREQSWEEP1 FREQSWEEP2 FSINTERFACE FSISCALING FRAMETYPE
%token GEPS GLOBALTOL GRAVITY GRBM GTGSOLVER GLOBALCRBMTOL
%token HDIRICHLET HEAT HFETI HNEUMAN HSOMMERFELD HFTT
%token HELMHOLTZ HNBO HELMMF HELMSO HSCBO HZEM HZEMFILTER HLMPC 
%token HELMSWEEP HELMSWEEP1 HELMSWEEP2
%token ATDDIR ATDNEU
%token IACC IDENTITY IDIS IDIS6 IntConstant INTERFACELUMPED ITEMP ITERTYPE IVEL 
%token INCIDENCE IHDIRICHLET IHDSWEEP IHNEUMANN ISOLVERTYPE
%token KRYLOVTYPE
%token LAYC LAYN LAYO LAYMAT LFACTOR LMPC LOAD LOBPCG LOCALSOLVER ARPACK JACOBI
%token MASS MATERIALS MAXITR MAXORTHO MAXVEC MODAL MPCPRECNO MPCPRECNOID MPCTYPE MPCTYPEID MPCSCALING 
%token MPCBLK_OVERLAP MFTT MPTT MRHS MULTIPLIERS
%token NEIGPA NEWMARK NewLine NL NLMAT NLPREC NOCOARSE NODES 
%token NSBSPV NLTOL NUMCGM NFRAMES
%token OPTIMIZATION OUTPUT OUTPUT6 
%token QSTATIC
%token PENALTY PITA PITADISP6 PITAVEL6 NOFORCE CONSTFORCE CKCOARSE MDPITA 
%token PRECNO PRECONDITIONER PRELOAD PRESSURE PRINTMATLAB PROJ PRIMALTOL PIVOT PRECTYPE PRECTYPEID
%token RBMFILTER RBMSET READMODE REBUILD RENUM RENUMBERID REORTHO RESTART RECONS RECONSALG 
%token SCALING SCALINGTYPE SENSORS SOLVERTYPE
%token SPOOLESTAU SPOOLESSEED SPOOLESMAXSIZE SPOOLESMAXDOMAINSIZE SPOOLESMAXZEROS SPARSEMAXSUP
%token STATS STRESSID SUBSPACE SURFACE SAVEMEMCOARSE SPACEDIMENSION SCATTERER STAGTOL SCALED
%token TANGENT TEMP TIME TOLEIG TOLFETI TOLJAC TOLPCG TOPFILE TOPOLOGY TRBM THERMOE THERMOH 
%token TETT TOLCGM TURKEL TIEDSURFACES THETA THIRDNODE
%token USE USERDEFINEDISP USERDEFINEFORCE UPROJ
%token VERSION WAVENUMBER YMTT 
%token ZERO OLDBINARY BINARY GEOMETRY DECOMPFILE GLOBAL MATCHER Matcher CONSISTENT CPUMAP
%token NODALCONTACT MODE FRIC GAP
%token SHIFT OUTERLOOP OUTERLOOPTYPE EDGEWS WAVETYPE ORTHOTOL IMPE FREQ DPH WAVEMETHOD
%token MATSPEC MATUSAGE MATTYPE READ
%token SURFACETOPOLOGY MORTARTIED SEARCHTOL STDMORTAR DUALMORTAR WETINTERFACE
%token NSUBS EXITAFTERDEC SKIPDECCALL OUTPUTMEMORY OUTPUTWEIGHT
%token WEIGHTLIST GMRESRESIDUAL

%type <complexFDBC> AxiHD
%type <complexFNBC> AxiHN
%type <axiMPC>   AxiLmpc
%type <bclist>   DirichletBC NeumanBC BCDataList IDisp6 TBCDataList PBCDataList AtdDirScatterer AtdNeuScatterer PitaIDisp6 PitaIVel6
%type <bclist>   TempDirichletBC TempNeumanBC TempConvection ModalValList
%type <bcval>    BC_Data MPCHeader TBC_Data ModalVal PBC_Data
%type <coefdata> CoefList
%type <cxbcval>  ComplexBC_Data ComplexMPCHeader
%type <mpcterm>  MPCLine ComplexMPCLine
%type <cxbclist> ComplexBCDataList ComplexNeumanBC ComplexDirichletBC 
%type <frame>    Frame
%type <nframe>   NodalFrame
%type <fval>     Float DblConstant
%type <ival>     AEROTYPE Attributes AUGMENTTYPE AVERAGED 
%type <ival>     COLLOCATEDTYPE CORNERTYPE COMPLEXOUTTYPE
%type <ival>     DOFTYPE
%type <ival>     FETIPREC FETI2TYPE FRAMETYPE
%type <ival>     GTGSOLVER Integer IntConstant ITERTYPE 
%type <ival>     RBMSET RENUMBERID 
%type <ival>     WAVETYPE WAVEMETHOD
%type <ival>     SCALINGTYPE SOLVERTYPE STRESSID SURFACE Pressure Consistent
%type <ival>     NodalContact OUTERLOOPTYPE
%type <ldata>    LayData LayoData LayMatData
%type <linfo>    LaycInfo LaynInfo LayoInfo
%type <mftval>   MFTTInfo
%type <mptval>   MPTTInfo
%type <hftval>   HFTTInfo
%type <nl>       NodeNums SommNodeNums 
%type <nval>     Node
%type <lmpcons>  MPCList ComplexMPCList
%type <strval>   FNAME 
%type <ymtt>     YMTTList
%type <ctett>    TETTList
%type <dlist>    FloatList
%type <SurfObj>  FaceSet
%type <MortarCondObj> MortarCondition TiedSurfaces
%type <ival>     MPCTYPEID MPCPRECNOID
%type <ival>     ISOLVERTYPE RECONSALG
%type <ival>     PRECTYPEID
%type <ival>     MATTYPE
%type <eval>     Element

%%
FinalizedData:
	All END
	 { 
          return 0;
         }
	;
All:
	Component
	| All Component
	;
Component:
	NodeSet 
	| DirichletBC
	| NeumanBC
        | LMPConstrain 
        | ComplexLMPConstrain 
	| ElemSet
	| FrameDList
        | NodalFrameDList
	| Attributes
	| Materials
	| Solver
	| Pressure
	| Consistent
        | Preload
	| Renumbering
	| IDisp
	| Mode
	| IDisp6
        | PitaIDisp6     
        | PitaIVel6      
        | NoForce        
        | ConstForce     
        | CkCoarse     
	| IVel
	| IAcc
	| ITemp
	| SensorLocations
	| ActuatorLocations
	| UsddLocations
	| UsdfLocations
        | DynInfo
        | QstaticInfo
	| OutInfo
        | NLInfo
	| DiscrMasses
	| Composites
	| Cframes
        | LayMat
	| MFTTInfo 
	| MPTTInfo
        | HFTTInfo
        | YMTTable
        | TETTable
	| RbmTolerance
        | ToleranceInfo
        | Gravity
	| RbmFilterInfo
	| HzemFilterInfo
        | Restart
	| LoadCInfo
	| UseCInfo
        | Control
        | Optimization
        | OrthoInfo
        | NewtonInfo
	| AeroInfo
        | AeroHeatInfo
        | ThermohInfo
        | ThermoeInfo
        | HzemInfo
	| CondInfo
	| MassInfo
	| TempDirichletBC
        | TempNeumanBC
        | TempConvection
	| TopInfo
        | OldHelmInfo
	| HelmInfo
        | HelmMFInfo
        | HelmSOInfo
        | Scatterer
        | FarFieldPattern
        | HelmHoltzBC
        | EleHelmHoltzBC
        | EleScatterer
        | NeumScatterer
        | HelmScatterer
        | ComplexNeumanBC
        | IComplexNeumannBC
        | ComplexDirichletBC
        | IComplexDirichletBC
        | IComplexDirichletBCSweep
        | AxiHDir
        | AxiHNeu
        | AxiNumModes
        | AxiNumSlices
        | AxiHSommer
        | AxiMPC
        | OldBinarySpec
        | BinarySpec
        | AtdDirScatterer
        | AtdNeuScatterer
	| AtdArbScatterer
	| AtdNeumScatterer
	| AtdRobinScatterer
        | Decompose
	| WeightList
	| NodalContact
        | ModeInfo
	| Shift
        | Impe
	| MatSpec
	| MatUsage
	| FaceSet
	| MortarCondition
        | TiedSurfaces
        | WetInterface
        | FSInterface
	| BoffsetList
	| ParallelInTimeInfo                                                    
        ;
Shift:
        SHIFT Float NewLine
	;
Impe:
        IMPE NewLine FREQ Float NewLine
        | IMPE NewLine FREQSWEEP1 Float Float Integer NewLine
        | IMPE NewLine FREQSWEEP2 Float Float Integer NewLine
        | IMPE NewLine FREQSWEEP Float Float Integer Integer NewLine
        | IMPE NewLine FreqSweep 
        | Impe ReconsInfo
        | Impe DampInfo
        ;
FreqSweep:  
        FREQSWEEP Float NewLine
        | FreqSweep Float Integer NewLine
        ;
ReconsInfo:
        RECONS RECONSALG NewLine
        | RECONS RECONSALG Integer NewLine
        | RECONS RECONSALG Integer Integer NewLine
        | RECONS RECONSALG Integer Integer Integer NewLine
        ;
OldBinarySpec:
        OLDBINARY NewLine
        | OldBinarySpec GEOMETRY FNAME NewLine
        | OldBinarySpec DECOMPFILE FNAME NewLine
        | OldBinarySpec GLOBAL FNAME NewLine
        | OldBinarySpec MATCHER FNAME NewLine
        | OldBinarySpec CPUMAP FNAME NewLine
	;
BinarySpec:
	BINARY NewLine
        ;
Decompose :
        DECOMPOSE NewLine
        | Decompose DECOMPFILE FNAME NewLine
        | Decompose NSUBS Integer NewLine
        | Decompose OUTPUTWEIGHT NewLine
        | Decompose OUTPUTMEMORY NewLine
        | Decompose EXITAFTERDEC NewLine
        | Decompose SKIPDECCALL NewLine
        | Decompose DETER NewLine
        ;
WeightList :
        WEIGHTLIST NewLine
        | WeightList Integer Float NewLine
        ;
MFTTInfo:
	MFTT NewLine {}
	| MFTTInfo Float Float NewLine
	;
MPTTInfo:
        MPTT NewLine {}
        | MPTTInfo Float Float NewLine
        ;
HFTTInfo:
        HFTT NewLine {}
        | HFTTInfo Float Float NewLine
        ;
Composites:
	COMPOSITE NewLine
	| Composites CoefInfo
	| Composites LaycInfo
	| Composites LaynInfo
        | Composites LayoInfo
	;
Cframes:
	CFRAMES NewLine
	| Cframes Frame
	;
CoefInfo:
	COEF Integer NewLine CoefList
	;
CoefList:
	Integer Integer Float NewLine {}
	| CoefList Integer Integer Float NewLine
	;
LaycInfo:
	LAYC Integer NewLine {}
	| LaycInfo LayData
	;
LaynInfo:
	LAYN Integer NewLine {}
	| LaynInfo LayData
	;
LayoInfo:
        LAYO Integer NewLine {}
        | LayoInfo LayoData
        ;
LayData:
	Integer Float Float Float Float Float Float Float Float Float NewLine {}
	;
LayoData:
        Integer Integer Float Float NewLine {}
        ;
LayMat:
        LAYMAT NewLine
        | LayMat LayMatData
        ;
LayMatData:
        Integer Float Float Float Float Float NewLine {}
        ;
DiscrMasses:
	DIMASS NewLine
	| DiscrMasses Integer Integer Float NewLine
	;
Gravity:
	GRAVITY NewLine
	| Gravity Float Float Float NewLine
	;
Restart:
	RESTART NewLine 
	| Restart FNAME FNAME NewLine
        | Restart FNAME Integer NewLine
	;
LoadCInfo:
	LOAD FNAME NewLine
	;
UseCInfo:
	USE FNAME NewLine
	;
SensorLocations:
        SENSORS NewLine BCDataList
	;
ActuatorLocations:
        ACTUATORS NewLine BCDataList
	;
UsdfLocations:
	USERDEFINEFORCE NewLine BCDataList
	;
UsddLocations:
	USERDEFINEDISP NewLine BCDataList
	;
OutInfo:
	OUTPUT NewLine
	| OUTPUT6 NewLine
	| OutInfo STRESSID FNAME Integer DOFTYPE NewLine
        | OutInfo STRESSID FNAME Integer Integer DOFTYPE NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer DOFTYPE NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE FNAME Integer DOFTYPE NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE Integer FNAME Integer DOFTYPE NewLine
        | OutInfo STRESSID FNAME Integer NewLine
	| OutInfo STRESSID Integer Integer FNAME Integer NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE FNAME Integer NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE Integer FNAME Integer NewLine
        | OutInfo STRESSID FNAME Integer Integer NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer Integer NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE FNAME Integer Integer NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE Integer FNAME Integer Integer NewLine
        | OutInfo STRESSID FNAME Integer AVERAGED NewLine
        | OutInfo STRESSID FNAME Integer AVERAGED SURFACE NewLine
	| OutInfo STRESSID FNAME Integer SURFACE NewLine
	| OutInfo STRESSID FNAME Integer AVERAGED Float Float NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer AVERAGED NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE FNAME Integer AVERAGED NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE Integer FNAME Integer AVERAGED NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer AVERAGED SURFACE NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE FNAME Integer AVERAGED SURFACE NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE Integer FNAME Integer AVERAGED SURFACE NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer AVERAGED Float Float NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE FNAME Integer AVERAGED Float Float NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE Integer FNAME Integer AVERAGED Float Float NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer SURFACE NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE FNAME Integer SURFACE NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE Integer FNAME Integer SURFACE NewLine
        | OutInfo STRESSID FNAME Integer Integer SURFACE NewLine
        | OutInfo STRESSID FNAME Integer Integer Float Float NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer Integer SURFACE NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE FNAME Integer Integer SURFACE NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE Integer FNAME Integer Integer SURFACE NewLine
	| OutInfo STRESSID Integer Integer FNAME Integer Integer Float Float NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE FNAME Integer Integer Float Float NewLine
        | OutInfo STRESSID COMPLEXOUTTYPE Integer FNAME Integer Integer Float Float NewLine
	;
DynInfo:
        DynamInfo
	| EIGEN NewLine
	| EIGEN Integer NewLine
	| NEIGPA Integer NewLine
	| SUBSPACE NewLine
        | SUBSPACE Integer Float Float NewLine
	| NSBSPV Integer NewLine
	| TOLEIG Float NewLine
	| TOLJAC Float NewLine
        | EXPLICIT NewLine
        | ARPACK NewLine
        | JACOBI Integer NewLine
        | LOBPCG NewLine
	;
MassInfo:
	MASS NewLine
	;
CondInfo:
	CONDITION NewLine Float NewLine
	| CONDITION NewLine
	;
TopInfo:
	TOPFILE NewLine
	;
DynamInfo:
        DYNAM NewLine 
	| DynamInfo TimeIntegration
        | DynamInfo TimeInfo
        | DynamInfo DampInfo
        | DynamInfo TempInfo
        | DynamInfo MODAL NewLine
	;
TimeIntegration:
        NEWMARK NewLine
        | NEWMARK Float Float NewLine
        | NEWMARK Float Float Float Float NewLine
        | NEWMARK Float NewLine
	| ACOUSTIC NewLine                                        
        | ACOUSTIC Float Float NewLine
        | ACOUSTIC Float Float Float Float NewLine
        | ACOUSTIC Float NewLine
	;
QstaticInfo:
        QSTATIC NewLine
        | QstaticInfo QMechInfo
        | QstaticInfo QHeatInfo
        | QstaticInfo MODAL NewLine
	;
QMechInfo:
        NEWMARK Float Float Integer NewLine
        |
        NEWMARK Float Float Integer Float NewLine
	;
QHeatInfo:
        HEAT Float Float Float Integer NewLine
        |
        NEWMARK Float Float Integer Float Float NewLine
	;
AeroInfo:
        AERO AEROTYPE NewLine
	| AERO NewLine AEROTYPE NewLine
        | AERO NewLine AEROTYPE Float NewLine
	| AERO NewLine AEROTYPE Float Float NewLine
	| AeroInfo COLLOCATEDTYPE NewLine
        | Matcher FNAME NewLine
	;
AeroHeatInfo:
        AEROH NewLine AEROTYPE Float Float NewLine
	;
ThermohInfo:
        THERMOH NewLine 
	;
ThermoeInfo:
        THERMOE NewLine 
	;
ModeInfo:
        MODE NewLine 
	;
HzemInfo:
        HZEM NewLine
	;
RbmTolerance:
        TRBM NewLine Float NewLine
        | TRBM NewLine Float Float NewLine
	;
ToleranceInfo:
        GRBM NewLine Float Float NewLine
        | GRBM NewLine Float NewLine
	;
RbmFilterInfo:
	RBMFILTER Integer NewLine
	| RBMFILTER NewLine
        | RBMFILTER NewLine RbmList NewLine
	;
RbmList:
        Integer
        | RbmList Integer
        ;
HzemFilterInfo:
        HZEMFILTER NewLine
	;
TimeInfo:
	TIME Float Float Float NewLine
	;
ParallelInTimeInfo: 
        PITA Integer Integer Integer NewLine
        | MDPITA Integer Integer Integer Integer NewLine
        ;
NoForce: 
        NOFORCE NewLine
        ; 
ConstForce:   
        CONSTFORCE NewLine 
        ; 
CkCoarse:
        CKCOARSE NewLine 
        ;
DampInfo:
	DAMPING Float Float NewLine
	| DAMPING MODAL NewLine ModalValList
	;
TempInfo:
        HEAT Float NewLine
	;
ComplexDirichletBC:
	HDIRICHLET NewLine ComplexBCDataList {}
	;
IComplexDirichletBC:
        IHDIRICHLET NewLine Float Float Float NewLine
        | IHDIRICHLET NewLine Float Float NewLine
        ;
IComplexDirichletBCSweep:
        IHDSWEEP NewLine Integer Float Float NewLine
        | IHDSWEEP Integer NewLine
        | IComplexDirichletBCSweep DirectionVector
        ;
DirectionVector:
        Integer Float Float Float NewLine
        ;
IComplexNeumannBC:
        IHNEUMANN NewLine Float Float Float NewLine
        ;
DirichletBC:
        DISP NewLine BCDataList {}
	;
TempDirichletBC:
        TEMP NewLine {}
        | TempDirichletBC Integer Float NewLine
	;
TempNeumanBC:
        FLUX NewLine {}
        | TempNeumanBC Integer Float NewLine
	;
TempConvection:
        CONVECTION NewLine {}
        | TempConvection Integer Float Float Float NewLine
	;
HelmHoltzBC:
        HSOMMERFELD NewLine SommerfeldBCDataList
	;
SommerfeldBCDataList:
        SommerfeldBC_Data
        | SommerfeldBCDataList SommerfeldBC_Data
	;
SommerfeldBC_Data:
        Integer Integer Float NewLine
        | Integer Integer Integer Float NewLine
        | Integer Integer Integer Integer Float NewLine
	;
EleHelmHoltzBC:
        ELHSOMMERFELD NewLine SommerElement
        | EleHelmHoltzBC SommerElement
        ;
SommerElement:
        Integer Integer SommNodeNums NewLine
        ;
SommNodeNums:
        Integer {}
        | SommNodeNums Integer
        ;
Scatterer:
        SCATTERER NewLine ScattererEleList
        ;
ScattererEleList:
        ScattererEle
        | ScattererEleList ScattererEle
        ;
ScattererEle:
        Integer Integer Float NewLine
        | Integer Integer Integer Float NewLine
        | Integer Integer Integer Integer Float NewLine
        ;
EleScatterer:
        ELSCATTERER NewLine ScatterElement
        | EleScatterer ScatterElement
        ;
ScatterElement:
        Integer Integer SommNodeNums NewLine
        ;
NeumScatterer:
        HNBO NewLine NeumElement
        | NeumScatterer NeumElement
        ;
NeumElement:
        Integer Integer SommNodeNums NewLine
        ;
HelmScatterer:
        HSCBO NewLine HelmScattererElement
        | HelmScatterer HelmScattererElement
        ;
HelmScattererElement:
        Integer Integer SommNodeNums NewLine
        ;
PBC_Data:
	Integer Float NewLine {}
	;
PBCDataList:
	PBC_Data {}
	| PBCDataList PBC_Data
	;
AtdDirScatterer:
        ATDDIR NewLine PBCDataList {}
        ;
AtdNeuScatterer:
        ATDNEU NewLine PBCDataList {}
        ;
AtdArbScatterer:
	ATDARB Float NewLine {}
        | AtdArbScatterer SommerElement
	;
AtdNeumScatterer:
	ATDDNB Float NewLine
        | AtdNeumScatterer NeumElement
        ;
AtdRobinScatterer:
	ATDROB Float Float Float NewLine
        | AtdRobinScatterer HelmScattererElement
        ;
FarFieldPattern:
        FFP Integer NewLine
        | FFP Integer Integer NewLine
        ;
AxiHDir:
        AXIHDIR NewLine Float Float NewLine Float Float Float NewLine
        | AxiHDir AxiHD
	;
AxiHD:
        Integer NewLine {}
	;
AxiHNeu:
        AXIHNEU NewLine Float Float NewLine Float Float Float NewLine
        | AxiHNeu AxiHN
	;
AxiHN:
        Integer Integer NewLine {}
        | Integer Integer Integer NewLine {}
	;
AxiNumModes:
        AXINUMMODES Integer NewLine
	;
AxiNumSlices:
        AXINUMSLICES Integer NewLine
	;
AxiHSommer:
        AXIHSOMMER NewLine Integer NewLine Float Float NewLine
        | AxiHSommer AxiHSData
	;
AxiHSData:
        Integer Integer NewLine
        | Integer Integer Integer NewLine
	;
AxiMPC:
        AXIMPC NewLine
        | AxiMPC AxiLmpc
	;
AxiLmpc:
        Integer Float Float Integer NewLine {}
        | Integer Float Float Integer Float Float NewLine {}
        | Integer Float Float Integer Float Float Float Float Float NewLine {}
	;
Mode:
	READMODE FNAME NewLine
	;
IDisp:
	IDIS NewLine BCDataList
	| IDIS ZERO NewLine
	| IDIS NewLine MODAL NewLine ModalValList
	;
IDisp6:
	IDIS6 Float NewLine {}
	| IDIS6 NewLine {}
	| IDisp6 Integer Float Float Float Float Float Float NewLine
        | IDisp6 Integer Float Float Float NewLine
	| GEPS NewLine {}
	| BUCKLE NewLine {}
	;
PitaIDisp6:
        PITADISP6 Integer NewLine {}
        | PitaIDisp6 Integer Float Float Float Float Float Float NewLine
        ;
PitaIVel6:
        PITAVEL6 Integer NewLine {}
        | PitaIVel6 Integer Float Float Float Float Float Float NewLine
        ;
IVel:
        IVEL NewLine BCDataList
	;
IAcc:
        IACC NewLine BCDataList
	;
ITemp:
        ITEMP NewLine TBCDataList
	;
NeumanBC:
	FORCE NewLine BCDataList {}
	;
BCDataList:
	BC_Data {}
	| BCDataList BC_Data
	;
ModalValList:
	ModalVal {}
	| ModalValList ModalVal
	;
TBCDataList:
	TBC_Data {}
	| TBCDataList TBC_Data
	;
YMTTable:
        YMTT NewLine
        | YMTT NewLine YMTTList
        ;
YMTTList:
        CURVE Integer NewLine Float Float NewLine {}
        | YMTTList Float Float NewLine
        | YMTTList CURVE Integer NewLine Float Float NewLine
        ;
TETTable:
        TETT NewLine
        | TETT NewLine TETTList
        ;
TETTList:
        CURVE Integer NewLine Float Float NewLine {}
        | TETTList Float Float NewLine
        | TETTList CURVE Integer NewLine Float Float NewLine
        ;
LMPConstrain:
        LMPC NewLine
        | LMPC NewLine MPCList
	;
MPCList:
        MPCHeader MPCLine {}
        | MPCList MPCLine
        | MPCList MPCHeader MPCLine
	;
MPCHeader:
        Integer Float NewLine {}
        | Integer NewLine {}
	;
MPCLine:
        Integer Integer Float NewLine {}
	;
ComplexLMPConstrain:
        HLMPC NewLine
        | HLMPC NewLine ComplexMPCList
        ;
ComplexMPCList:
        ComplexMPCHeader ComplexMPCLine {}
        | ComplexMPCList ComplexMPCLine
        | ComplexMPCList ComplexMPCHeader ComplexMPCLine
        ;
ComplexMPCHeader:
        Integer CRHS Float Float NewLine {}
        | Integer Float NewLine {}
        | Integer NewLine {}
        ;
ComplexMPCLine:
        Integer Integer Float Float NewLine {}
        | Integer Integer Float NewLine {}
        ;
ComplexNeumanBC:
	HNEUMAN NewLine ComplexBCDataList {}
	;
ComplexBCDataList:
	ComplexBC_Data {}
        | ComplexBCDataList ComplexBC_Data
	;
Materials:
	MATERIALS NewLine MatData
	| Materials MatData
	;
MatData:
	Integer Float Float Float Float Float Float Float Float Float Float Float Float Float Float NewLine
	| Integer Float Float Float Float Float Float Float Float Float Float Float Float Float Float Float Float Float Float NewLine
        | Integer Float Float Float Float Float Float Float NewLine
        | Integer Float Float Float Float Float Float Float Float Float Float Float Float NewLine
        | Integer CONSTRMAT NewLine
        | Integer CONSTRMAT ConstraintOptionsData NewLine
        | Integer MASS Float Float Float Float Float Float Float Float Float Float NewLine
        | Integer CONSTRMAT MASS Float NewLine
        | Integer CONSTRMAT ConstraintOptionsData MASS Float NewLine
        | Integer CONSTRMAT MASS Float Float NewLine
        | Integer CONSTRMAT ConstraintOptionsData MASS Float Float NewLine
        | Integer CONSTRMAT Integer Float Float Float Float Float Float Integer NewLine
	;
ConstraintOptionsData:
        MULTIPLIERS
        | PENALTY Float
        ;
ElemSet:
        TOPOLOGY NewLine Element
        { domain->addElem($3.num,$3.elem); }
        | ElemSet Element
        { domain->addElem($2.num,$2.elem); }
	;
FaceSet:
	SURFACETOPOLOGY Integer NewLine {}
        | FaceSet Integer Integer NodeNums NewLine 
	; 
MortarCondition:
	MORTARTIED Integer Integer NewLine {}
	| MORTARTIED Integer Integer STDMORTAR NewLine {}
	| MORTARTIED Integer Integer DUALMORTAR NewLine {}
	| MORTARTIED Integer Integer SEARCHTOL Float NewLine {}
	| MORTARTIED Integer Integer SEARCHTOL Float Float NewLine {}
	| MORTARTIED Integer Integer STDMORTAR SEARCHTOL Float NewLine {}
	| MORTARTIED Integer Integer STDMORTAR SEARCHTOL Float Float NewLine {}
	| MORTARTIED Integer Integer DUALMORTAR SEARCHTOL Float NewLine {}
	| MORTARTIED Integer Integer DUALMORTAR SEARCHTOL Float Float NewLine {}
	;
TiedSurfaces:
        TIEDSURFACES NewLine {}
        | TiedSurfaces Integer Integer Integer NewLine
        ;
WetInterface:
        WETINTERFACE Integer Integer NewLine
        | WETINTERFACE Integer NewLine
        ;
FSInterface:
        FSINTERFACE NewLine
        | FSInterface Integer Integer Integer NewLine
        ;
NodeSet:
        NODES NewLine Node
        { domain->addNode($3.num, $3.xyz, $3.cp); }
        | NodeSet Node
        { domain->addNode($2.num, $2.xyz, $2.cp); }
        ;
Node:
        Integer Float Float Float NewLine
        { $$.num = $1-1; $$.xyz[0] = $2; $$.xyz[1] = $3;  $$.xyz[2] = $4;  $$.cp = 0; }
        | Integer Float Float NewLine
        { $$.num = $1-1; $$.xyz[0] = $2; $$.xyz[1] = $3;  $$.xyz[2] = 0.0; $$.cp = 0; }
        | Integer Float NewLine
        { $$.num = $1-1; $$.xyz[0] = $2; $$.xyz[1] = 0.0; $$.xyz[2] = 0.0; $$.cp = 0; }
        | Integer Float Float Float Integer NewLine
        { $$.num = $1-1; $$.xyz[0] = $2; $$.xyz[1] = $3;  $$.xyz[2] = $4;  $$.cp = $5;
          if($5 != 0) domain->basicPosCoords = false; }
        | Integer Float Float Float Integer Integer NewLine
        { $$.num = $1-1; $$.xyz[0] = $2; $$.xyz[1] = $3;  $$.xyz[2] = $4;  $$.cp = $5;
          if($5 != 0) domain->basicPosCoords = false; }
	;
Element:
        Integer Integer NodeNums NewLine
        { $$.num = $1-1;
          try{ $$.elem = buildElem($2,$3.num,$3.nd); }
          catch(const char *txt)
           {
              fprintf(stderr, "Element %d error: %s\n", $1, txt);
           }
        }
	;
NodeNums:
        Integer
        { $$.num = 1; $$.nd[0] = $1-1; }
        | NodeNums Integer
        { if($$.num == 64) return -1;
          $$.nd[$$.num] = $2-1; $$.num++; }
	;
BC_Data:
	Integer Integer Float NewLine {}
	| Integer Integer NewLine {}
	;
ModalVal:
	Integer Float NewLine {}
	;
TBC_Data:
	Integer Float NewLine {}
	;
ComplexBC_Data:
	Integer Integer Float Float NewLine {}
	| Integer Integer Float NewLine {}
	;
FrameDList:
	EFRAMES NewLine Frame
	| FrameDList Frame
	;
Frame:
	Integer Float Float Float Float Float Float Float Float Float NewLine {}
        | Integer THIRDNODE Integer NewLine {}
	;
NodalFrameDList:
        NFRAMES NewLine
        | NodalFrameDList NodalFrame
        { domain->setNodalFrame($2.id,$2.o,$2.d,$2.type); }
        ;
NodalFrame:
        Integer Float Float Float Float Float Float Float Float Float NewLine
        { $$.id = $1;
          $$.type = NFrameData::Rectangular;
          $$.o[0] = 0;  $$.o[1] = 0;  $$.o[2] = 0;
          $$.d[0] = $2; $$.d[1] = $3; $$.d[2] = $4;
          $$.d[3] = $5; $$.d[4] = $6; $$.d[5] = $7;
          $$.d[6] = $8; $$.d[7] = $9; $$.d[8] = $10; }
        | Integer Float Float Float Float Float Float Float Float Float Float Float Float NewLine
        { $$.id = $1;
          $$.type = NFrameData::Rectangular;
          $$.o[0] = $2;  $$.o[1] = $3;  $$.o[2] = $4;
          $$.d[0] = $5;  $$.d[1] = $6;  $$.d[2] = $7;
          $$.d[3] = $8;  $$.d[4] = $9;  $$.d[5] = $10;
          $$.d[6] = $11; $$.d[7] = $12; $$.d[8] = $13; }
        | Integer FRAMETYPE Float Float Float Float Float Float Float Float Float NewLine
        { $$.id = $1;
          $$.type = $2;
          $$.o[0] = 0;  $$.o[1] = 0;   $$.o[2] = 0;
          $$.d[0] = $3; $$.d[1] = $4;  $$.d[2] = $5;
          $$.d[3] = $6; $$.d[4] = $7;  $$.d[5] = $8;
          $$.d[6] = $9; $$.d[7] = $10; $$.d[8] = $11; }
        | Integer FRAMETYPE Float Float Float Float Float Float Float Float Float Float Float Float NewLine
        { $$.id = $1;
          $$.type = $2;
          $$.o[0] = $3;  $$.o[1] = $4;  $$.o[2] = $5;
          $$.d[0] = $6;  $$.d[1] = $7;  $$.d[2] = $8;
          $$.d[3] = $9;  $$.d[4] = $10; $$.d[5] = $11;
          $$.d[6] = $12; $$.d[7] = $13; $$.d[8] = $14; }
        ;
BoffsetList:
	BOFFSET NewLine
	| BoffsetList Integer Integer Float Float Float NewLine
	;
Attributes:
	ATTRIBUTES NewLine {}
	| Attributes Integer Integer NewLine
	| Attributes Integer Integer Integer Integer NewLine
        | Attributes Integer Integer Integer THETA Float NewLine
	| Attributes Integer Integer IDENTITY NewLine
	| Attributes Integer Integer Integer NewLine
	| Attributes Integer Integer Integer Integer Integer NewLine
        | Attributes Integer Integer Integer Integer THETA Float NewLine
	;
Pressure:
	PRESSURE NewLine {}
	| Pressure Integer Float NewLine
	| Pressure Integer Integer Float NewLine
	;
Consistent:
	CONSISTENT NewLine {}
	;
Preload:
        PRELOAD NewLine
        | Preload Integer Float NewLine
        | Preload Integer Integer Float NewLine
	;
Solver:
	STATS NewLine DIRECT NewLine
	| STATS NewLine DIRECT Integer NewLine
	| STATS NewLine SOLVERTYPE NewLine
        | STATS NewLine SOLVERTYPE PIVOT NewLine
	| STATS NewLine ITERTYPE Integer Float Integer NewLine
	| STATS NewLine ITERTYPE Integer Float Integer Integer NewLine
        | STATS NewLine ITERTYPE NewLine PRECNO Integer NewLine TOLPCG Float NewLine MAXITR Integer NewLine
        | STATS NewLine FETI Integer Float NewLine
        | STATS NewLine FETI Integer NewLine
	| STATS NewLine FETI DP NewLine
        | STATS NewLine FETI DPH NewLine
        | STATS NewLine FETI Integer FETI2TYPE NewLine
        | STATS NewLine FETI Integer Float Integer NewLine
        | FETI Integer Float Integer NewLine
	| STATS NewLine FETI NewLine
	| FETI NewLine
	| FETI Integer NewLine
	| FETI DP NewLine
        | FETI DPH NewLine
	| FETI Integer FETI2TYPE NewLine
        | SPARSEMAXSUP Integer NewLine
        | SPOOLESTAU Float NewLine
        | SPOOLESMAXSIZE Integer NewLine
        | SPOOLESMAXDOMAINSIZE Integer NewLine
        | SPOOLESSEED Integer NewLine
        | SPOOLESMAXZEROS Float NewLine
	| MAXITR Integer NewLine 
        | FETIPREC NewLine
	| PRECNO FETIPREC NewLine
        | PRECNO Integer NewLine
        | PRECTYPE Integer NewLine
        | PRECTYPE PRECTYPEID NewLine
        | DUALTOL Float NewLine
        | PRIMALTOL Float NewLine
        | TOLFETI Float NewLine
	| MAXORTHO Integer NewLine
	| NOCOARSE NewLine
	| PROJ Integer NewLine
	| SCALING Integer NewLine
	| SCALING SCALINGTYPE NewLine
        | MPCSCALING Integer NewLine
        | MPCSCALING SCALINGTYPE NewLine
        | FSISCALING Integer NewLine
        | FSISCALING SCALINGTYPE NewLine
	| CORNER CORNERTYPE NewLine
        | AUGMENT AUGMENTTYPE NewLine
        | AUGMENT AUGMENTTYPE RBMSET NewLine
        | AUGMENT EDGEWS Integer NewLine
        | AUGMENT EDGEWS WAVETYPE Integer NewLine
        | AUGMENT EDGEWS Integer WAVEMETHOD NewLine
        | AUGMENT EDGEWS WAVETYPE Integer WAVEMETHOD NewLine
	| ORTHOTOL Float NewLine
        | ORTHOTOL Float Float NewLine
	| GLOBALTOL Float NewLine
        | GLOBALCRBMTOL Float NewLine
        | CCTTOL Float NewLine
        | FPROJ Integer NewLine
        | EPROJ Integer NewLine
        | UPROJ Integer NewLine
        | STAGTOL Float NewLine
	| PRINTMATLAB NewLine
	| LOCALSOLVER SOLVERTYPE NewLine
	| COARSESOLVER SOLVERTYPE NewLine
        | CCTSOLVER SOLVERTYPE NewLine
        | LOCALSOLVER SOLVERTYPE PIVOT NewLine
        | LOCALSOLVER SOLVERTYPE SCALED NewLine
        | COARSESOLVER SOLVERTYPE PIVOT NewLine
        | COARSESOLVER SOLVERTYPE SCALED NewLine
        | CCTSOLVER SOLVERTYPE PIVOT NewLine
        | CCTSOLVER SOLVERTYPE SCALED NewLine
	| VERSION Integer NewLine
	| GTGSOLVER NewLine
        | GMRESRESIDUAL NewLine
        | ITERTYPE NewLine
        | FRONTAL NewLine
        | NLPREC NewLine
	| NLPREC Integer NewLine
        | HFETI Integer Float NewLine
        | HFETI Integer Float Integer NewLine
        | HFETI Integer Float Integer Float NewLine
        | HFETI Integer Float Integer Float Integer NewLine
        | HFETI NewLine
        | NUMCGM Integer NewLine
        | NUMCGM Integer Float NewLine
        | TOLCGM Float NewLine
        | SPACEDIMENSION Integer NewLine
        | KRYLOVTYPE Integer NewLine
        | KRYLOVTYPE ISOLVERTYPE NewLine
        | INTERFACELUMPED NewLine
        | SAVEMEMCOARSE NewLine
        | TURKEL Integer NewLine
        | TURKEL Integer Float NewLine
        | TURKEL Integer Float Float NewLine
        | OUTERLOOP OUTERLOOPTYPE NewLine
        | MPCTYPE Integer NewLine
        | MPCTYPE MPCTYPEID NewLine
        | MPCPRECNO Integer NewLine
        | MPCPRECNO Integer MPCBLK_OVERLAP Integer NewLine
        | MPCPRECNO MPCPRECNOID NewLine
        | MPCPRECNO MPCPRECNOID MPCBLK_OVERLAP Integer NewLine
        | MRHS Integer NewLine
	;
OldHelmInfo:
        FACOUSTICS NewLine FAcousticData
	;
FAcousticData:
        Float NewLine
	;
HelmInfo:
        HELMHOLTZ NewLine
        | FETIH NewLine
        | WAVENUMBER Float NewLine
        | WAVENUMBER Float Float Float NewLine
        | BGTL Integer NewLine
        | BGTL Integer Float NewLine
        | BGTL Integer Float Float NewLine
        | INCIDENCE Integer NewLine IncidenceList
        | HELMHOLTZ NewLine HELMSWEEP1 Float Float Integer NewLine
        | HELMHOLTZ NewLine HELMSWEEP2 Float Float Integer NewLine
        | HELMHOLTZ NewLine HELMSWEEP Float Float Integer Integer NewLine
        | HELMHOLTZ NewLine HelmSweep
        | HELMHOLTZ NewLine HELMSWEEP1 Float Float Integer Float Float NewLine
        | HELMHOLTZ NewLine HELMSWEEP2 Float Float Integer Float   Float NewLine
        | HELMHOLTZ NewLine HELMSWEEP  Float Float Integer Integer Float   Float NewLine
        | HelmInfo DampInfo
        | HelmInfo ReconsInfo 
        ;
HelmSweep:    
        HELMSWEEP Float NewLine
        | HELMSWEEP Float Float   Float NewLine
        | HelmSweep Float Integer NewLine
        ;
IncidenceList:
        IncidenceVector
        | IncidenceList IncidenceVector
        ;
IncidenceVector:
        Float Float Float NewLine
        ;
HelmMFInfo:
        HELMMF NewLine FAcousticDataMF
        ;
FAcousticDataMF:
        Float NewLine
        ;
HelmSOInfo:
        HELMSO NewLine FAcousticDataSO
        ;
FAcousticDataSO:
        Float NewLine
        ;
NLInfo:
        NL NewLine MAXITR Integer NewLine NLTOL Float NewLine
	| NL Integer NewLine MAXITR Integer NewLine NLTOL Float NewLine MAXVEC Integer NewLine DLAMBDA Float NewLine LFACTOR Float NewLine
	| NL NewLine ARCLENGTH NewLine MAXITR Integer NewLine NLTOL Float NewLine MAXVEC Integer NewLine DLAMBDA Float NewLine LFACTOR Float NewLine
	| NL NewLine MAXITR Integer NewLine NLTOL Float NewLine MAXVEC Integer NewLine DLAMBDA Float NewLine LFACTOR Float NewLine
        | NLInfo TimeInfo
	| DLAMBDA Float Float NewLine
	| NLInfo NLMAT NewLine
	;
NewtonInfo:
        REBUILD Integer NewLine
	| REBUILD Integer Integer NewLine
	| REBUILD NewLine TANGENT Integer NewLine
	| REBUILD NewLine TANGENT Integer NewLine PRECONDITIONER Integer NewLine
	| FITALG Integer NewLine
	| FITALG Integer Integer NewLine
	| NLTOL Float NewLine
        | NLTOL Float Float NewLine
	;
OrthoInfo:
	REORTHO NewLine
	;
Control:
	CONTROL NewLine
        | CONTROL NewLine FNAME NewLine Integer NewLine FNAME NewLine FNAME NewLine
        | CONTROL NewLine FNAME NewLine Integer NewLine FNAME NewLine FNAME NewLine FNAME NewLine
	;
Optimization:
	OPTIMIZATION FNAME NewLine
        | Optimization NewLine FNAME NewLine
	;
NodalContact: 
	NODALCONTACT Integer NewLine {}
        | NODALCONTACT Integer MODE Integer NewLine {}
        | NODALCONTACT Integer FRIC Float NewLine {}
	| NodalContact Integer Integer Float Float Float NewLine
	| NodalContact Integer Integer Float Float Float Float NewLine
	| NodalContact Integer Integer Float Float Float GAP Float NewLine
	| NodalContact Integer Integer Float Float Float MODE Integer NewLine
	| NodalContact Integer Integer Float Float Float MODE Integer GAP Float NewLine
	| NodalContact Integer Integer Float Float Float FRIC Float NewLine
	| NodalContact Integer Integer Float Float Float GAP Float FRIC Float NewLine
	;
MatSpec:
	MATSPEC NewLine
        | MatSpec Integer MATTYPE Float NewLine
        | MatSpec Integer MATTYPE Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float Float Float Float Float Float NewLine
	| MatSpec READ FNAME FNAME NewLine
	| MatSpec Integer FNAME FloatList NewLine
	;
MatUsage:
	MATUSAGE NewLine
	| MatUsage Integer Integer NewLine
	| MatUsage Integer Integer Integer NewLine
	;
FloatList:
        { $$.nval = 0; }
        | FloatList Float
        {
          if($1.nval == 32) {
             fprintf(stderr, "You'd better invent another material model!\n");
             exit(-1);
          }
          $$ = $1;
          $$.v[$$.nval++] = $2;
        }
	;
Renumbering:
	RENUM NewLine RENUMBERID NewLine
        | RENUM NewLine RENUMBERID NewLine RENUMBERID NewLine
	;
Integer:
	IntConstant
        { $$ = $1; }
	;
Float:
	IntConstant
        { $$ = $1; }
	| DblConstant 
        { $$ = $1; }
	;
%%
