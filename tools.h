#ifndef _TOOLS_
#define _TOOLS_

#include <cmath>

#ifdef OLD_STL
#include <algo.h>
#else
#include <algorithm>
using std::min;
using std::max;
#endif

#endif
