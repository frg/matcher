#ifndef _CONNECTIVITY_H_
#define _CONNECTIVITY_H_

class ElemSet;
class FaceSet;

struct compStruct {
  int numComp; // number of components
  int *xcomp;  // pointer to renum for the beginning of each component 
  int *order;  // order of the nodes
  int *renum;  // renumbering
};


class Connectivity {
  
  int size;           // size of pointer
  int numtarget;      // size of target
  int *pointer;       // pointer to target
  int *target;        // value of the connectivity
  float *weight;      // weights of pointer (or NULL)
protected :
  
  Connectivity(){}
  
public:
  Connectivity(ElemSet *elems);
  Connectivity(FaceSet *faces);
  Connectivity(int _size, int *_pointer, int *_target);
  Connectivity(int _size, int *_count);
  ~Connectivity();
  
  int *operator[](int i);
  int csize();
  int numConnect(); // Total number of connections
  int num(int);
  int num(int, int*);
  void print(char *fn);
  void print(FILE *f);
  int offset(int i,int j); // returns a unique id for connection i to j
  Connectivity* reverse(float * w= 0) ; // creates t->s from s->t
  Connectivity* transcon( Connectivity* );

  void findPseudoDiam(int *n1, int *n2, int *mask=0);
  int  rootLS(int root, int *xls, int *ls, int &w, int *mask=0);
  // Create a rooted level structure
  int *renumSloan(int *mask, int &firstNum, int *ren = 0);
  int *renumSloan();
  compStruct renumByComponent(int);
};

inline int
Connectivity::csize() { return size; }

inline int
Connectivity::num(int n) { return pointer[n+1] - pointer[n]; }

inline int *
Connectivity::operator[](int i) { return target +pointer[i] ; }

inline int
Connectivity::numConnect() { return numtarget; }

#endif
