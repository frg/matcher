#include"Decomp2D.h"

// stores the name of the input fluid mesh name
// from the decomp file, this is used when match.dec 
// is output for TOPDOMDEC
char meshName[80];

// global variable used as a boolean, 0 indicates 
// we wish to use an overlap, 1 means no overlap
int noOverlapGlobal; 

//------------------------------------------------------------------------------

Decomp2D::Decomp2D() {

  fluidDom = new FluidDomain2D;
  
  connectedDomain = NULL; 
  master     = NULL;
  nConnect   = NULL;
  subToTri   = NULL; 
  subToNode  = NULL;
  nodeToSub  = NULL;
  subToSub   = NULL;
  interfNode = NULL;

}

//------------------------------------------------------------------------------

Decomp2D::~Decomp2D() {

  delete fluidDom;

  if (connectedDomain)
    delete [] connectedDomain[0];

  if (interfNode)
    for (int i=0; i<numSub; i++)
      delete interfNode[i];

  delete [] connectedDomain;
  delete [] master;
  delete [] nConnect;
  delete [] subToTri;
  delete [] subToNode;
  delete [] nodeToSub;
  delete [] subToSub;
  delete [] interfNode;

}

//------------------------------------------------------------------------------

void Decomp2D::getFluidDomain(char* name) {

  fluidDom->getGeometry(name);

}

//------------------------------------------------------------------------------

void Decomp2D::getDecomp(char* name) {
  int i;
  int nele;

  if(name!=NULL) {
    ifstream decompFilePtr(name,ios::in);
    if(!decompFilePtr) {
      cerr << "Error opening file: " << name <<endl;
      cerr << "DONE" << endl;
      decompFilePtr.close();
      exit(1);
    }

    // The input file is scanned for the word "for" then the
    // name of the mesh is copied for use in output in the new
    // decomposition file.
    decompFilePtr>>meshName;	
    while (strncmp(meshName,"for",4))
      decompFilePtr>>meshName;
    decompFilePtr>>meshName;
    decompFilePtr>>numSub;

    // Now the subdomains are read in.
    cerr << "Number of subdomains :" << numSub << endl;

    // Check to see the number of subs is positive (duh), and
    // less then 99999 the max number allowed for in the
    // flu- files (because of the name flu-#####)
    if(99999<numSub||numSub<0) {
      cerr<<"The number of subdomains must be positive and can't ";
      cerr<<"exceed 99999."<<endl;
      cerr<<"DONE"<<endl;
      exit(1);
    }
    
    int *cx = new int[numSub+1];
    int *connect = new int[fluidDom->faces.size()];
    int  curEle = 0;
    for(int isub=0; isub < numSub; ++isub) {
      decompFilePtr>>nele;
      cerr << "Subdomain " << isub+1 << " has " << nele << "Elements\n";
      cx[isub] = curEle;

      if(curEle + nele > fluidDom->faces.size()) {
        cerr<<"This decomposition contains more elements than the ";
        cerr<<"original mesh."<<endl;
	cerr<<"DONE"<<endl;
        exit(1);
      }

      for(int iele=0; iele < nele; ++iele) {
        decompFilePtr>>connect[curEle];
        connect[curEle++]--;
      }
    }

    cx[numSub] = curEle;
    subToTri = new Connectivity(numSub,cx,connect);

  } else {
    // if there is no decomp filename given we create subToTri
    // ourselves with one sub and all elements in it
    numSub=1;

    int* cx=new int[2];
    int* connect=new int[fluidDom->faces.size()];

    cx[0]=0;
    cx[1]=fluidDom->faces.size();

    for(i=0;i<fluidDom->faces.size();i++)
      connect[i]=i;

    subToTri = new Connectivity(numSub,cx,connect);
  }
}

//------------------------------------------------------------------------------

void Decomp2D::getDomainConnect() {

  // create each subdomain's intertri lists
  int iSub, jSub, subJ, iNode;
  int totConnect;

  if(numSub>1){
    int *flag = new int[numSub];
    nConnect  = new int[numSub];

    for(iSub=0; iSub < numSub; ++iSub) {
      flag[iSub] = -1;
      nConnect[iSub] = 0;
    }
    
    // Count connectivity
    totConnect = 0;
    for(iSub=0; iSub < numSub; ++iSub) {
      
      // loop over the nodes connected to the current subdomain
      for(iNode = 0; iNode < subToNode->num(iSub); ++iNode) { 
        int thisNode = (*subToNode)[iSub][iNode];

	// loop over the subdomains connected to this Node
        for(jSub = 0; jSub < nodeToSub->num(thisNode); ++jSub) 
          if( (subJ=(*nodeToSub)[thisNode][jSub]) > iSub) {
	    // we only deal with connection to highered numbered 
	    // subdomains so that we guarantee symmetry of lists
	    if(flag[subJ] != iSub) {
	      flag[subJ] = iSub;
	      nConnect[iSub]++;
	      nConnect[subJ]++;
	      totConnect += 2;
	    }
	  }
      }
    }
    
    // Allocate memory for list of connected subdomains
    int **nodeCount  = new int*[numSub];
    int **remoteID   = new int*[numSub];
    int *whichLocal  = new int[numSub];
    int *whichRemote = new int[numSub];

    // This pointer to as list of pointers
    // is used to construct a 2D array 
    // containing the subdomains that touch
    // a particular subdomain.  Both nConnect
    // and connectedDoamin are necessary to
    // maintain the ordering used in interfNode
    connectedDomain = new int*[numSub];
    connectedDomain[0] = new int[2*totConnect];

    // Set pointers
    int *allAlloc = connectedDomain[0];
    for(iSub=0; iSub < numSub; ++iSub) {
      connectedDomain[iSub] = allAlloc;
      allAlloc += nConnect[iSub];

      remoteID[iSub] = allAlloc;
      allAlloc += nConnect[iSub];

    }

    for(iSub=0; iSub < numSub; ++iSub) {
      nodeCount[iSub] = new int[nConnect[iSub]];

      // Reset flag and nConnect for subdomain iSub
      flag[iSub] = -1;
      nConnect[iSub] = 0;      
    }
    
    for(iSub=0; iSub < numSub; ++iSub) {
      for(iNode = 0; iNode < subToNode->num(iSub); ++iNode) {
        int nd = (*subToNode)[iSub][iNode];
        for(jSub = 0; jSub < nodeToSub->num(nd); ++jSub)
          if( (subJ=(*nodeToSub)[nd][jSub]) > iSub){ 
	    // we only deal with connection to highered numbered subs

            if(flag[subJ] != iSub) { 
	      // attribute location for this sub
              flag[subJ] = iSub;
              connectedDomain[subJ][nConnect[subJ]] = iSub;
              connectedDomain[iSub][nConnect[iSub]] = subJ;
              remoteID[subJ][nConnect[subJ]] = nConnect[iSub];
              remoteID[iSub][nConnect[iSub]] = nConnect[subJ];
              whichLocal[subJ] = nConnect[iSub]++;
              whichRemote[subJ] = nConnect[subJ]++;
              nodeCount[iSub][whichLocal[subJ]]=1;
              nodeCount[subJ][whichRemote[subJ]]=1;
            } else {
              nodeCount[iSub][whichLocal[subJ]]++;
              nodeCount[subJ][whichRemote[subJ]]++;
            }
          }
      }
    }
    
    // allocate memory for intertri node lists
    interfNode = new Connectivity *[numSub];

    // interfNode[iSub] is a connectivity for a subdomain to its
    // intertri node (so it is per neighboring subdomain as in:
    // interfNode[3][2][0] is the first node shared by subdomain 3 with
    // its neighbor number two (third neighbor)
    // Similarly, interfNode[3][2]->num() is the number of nodes shared
    // bewteen 3 and 2    
    for(iSub=0; iSub < numSub; ++iSub)
      interfNode[iSub] = new Connectivity(nConnect[iSub], nodeCount[iSub]);
    
    // fill the lists
    for(iSub=0; iSub < numSub; ++iSub) {
      flag[iSub] = -1;
      nConnect[iSub] = 0;
    }

    for(iSub=0; iSub < numSub; ++iSub) {
      for(iNode = 0; iNode < subToNode->num(iSub); ++iNode) {
        int nd = (*subToNode)[iSub][iNode];
        for(jSub = 0; jSub < nodeToSub->num(nd); ++jSub)
          if( (subJ=(*nodeToSub)[nd][jSub]) > iSub){ 
	   // we only deal with connection to highered numbered subs

            if(flag[subJ] != iSub) {
	      // attribute location for this sub
              flag[subJ] = iSub;
              whichLocal[subJ] = nConnect[iSub]++;
              whichRemote[subJ] = nConnect[subJ]++;
              (*interfNode[iSub])[whichLocal[subJ]][0] = nd;
              (*interfNode[subJ])[whichRemote[subJ]][0] = nd;
              nodeCount[iSub][whichLocal[subJ]]=1;
              nodeCount[subJ][whichRemote[subJ]]=1;
            } else {
              int il = nodeCount[iSub][whichLocal[subJ]]++;
              int jl = nodeCount[subJ][whichRemote[subJ]]++;
              (*interfNode[iSub])[whichLocal[subJ]][il]  = nd;
              (*interfNode[subJ])[whichRemote[subJ]][jl] = nd;
            }
          }
      }
    }
    // nConnect [iSub] is the number of neighbors  for sub iSub
    // connectedDomain[iSub][jSub] is the subdomain number that is the
    // jSub'th neighbor to subdomain iSub
    
    // Free allocated memory
    delete [] nodeCount;
    delete [] remoteID;
    delete [] whichLocal;
    delete [] whichRemote;
    delete [] flag;

  } else {
    nConnect = new int[1];
    nConnect[0] = 0;
  }

}

//------------------------------------------------------------------------------

// routine smoothDown adds smoothNum layers of overlap then
// gives each element to the lowest number sub that posses it 
Connectivity* 
Decomp2D::smoothDown(int smoothNum, Connectivity* triToTri){
  int i,j;

  for(i=0; i<smoothNum; i++) {
    // Keep track of old subToTri connectivity
    Connectivity *subToTri_old = subToTri;
    
    // Build new subToTri connectivity
    subToTri = subToTri->transcon(triToTri);

    // Delete old one
    delete subToTri_old;
  }

  Connectivity* triToSub = subToTri->reverse();
  int* cx = new int[fluidDom->faces.size()+1];
  int* connect = new int[(fluidDom->faces.size())*numSub];
  int minSub;
  int count=0;

  for(i=0;i<fluidDom->faces.size();i++){
    cx[i] = count;
    minSub = numSub;

    for(j=0; j<triToSub->num(i); j++)
      minSub = min(minSub,(*triToSub)[i][j]);

    connect[count++] = minSub;
  }
  cx[fluidDom->faces.size()] = count;

  // Delete connectivities before we recreate them
  delete triToSub;
  delete subToTri;

  triToSub = new Connectivity(fluidDom->faces.size(),cx,connect);
  subToTri = triToSub->reverse();

  return triToSub;
}

//------------------------------------------------------------------------------

// routine smoothUp creates an oveerlap just like smoothdown and gives elements
// the larget number sub that posses them
// Together these routines are used to smooth out the decomposition removing
// most subdomain areas that are too thin
Connectivity*
Decomp2D::smoothUp(int smoothNum, Connectivity* triToTri){
  int i,j;

  for(i=0;i<smoothNum;i++) {
    // Keep track of old subToTri connectivity
    Connectivity *subToTri_old = subToTri;
    
    // Build new subToTri connectivity
    subToTri = subToTri->transcon(triToTri);

    // Delete old one
    delete subToTri_old;
  }

  Connectivity* triToSub = subToTri->reverse();
  int* cx = new int[fluidDom->faces.size()+1];
  int* connect = new int[(fluidDom->faces.size())*numSub];
  int maxSub;
  int count=0;

  for(i=0;i<fluidDom->faces.size();i++){
    cx[i]=count;
    maxSub=-1;
    for(j=0;j<triToSub->num(i);j++)
      maxSub=max(maxSub,(*triToSub)[i][j]);
    connect[count++]=maxSub;
  }
  cx[fluidDom->faces.size()]=count;

  // Delete connectivities before we recreate them
  delete triToSub;
  delete subToTri;

  triToSub = new Connectivity(fluidDom->faces.size(),cx,connect);
  subToTri = triToSub->reverse();

  return triToSub;
}

//------------------------------------------------------------------------------

void
Decomp2D::makeSubD(int noOverlap) {
  int* connect;
  int* cx;
  int i,j,k,count;

  noOverlapGlobal=noOverlap;

  // if we have multiple subdomains then we smooth them out and create a three
  // element overlap 
  if(numSub>1) {

    // the number of layers we wish to smooth with in most situations 
    // we will smooth out (remove) any subdomain area which will be 
    // entirely covered by the overlap
    int smoothNum = 3;

    // triangle to node connectivity is created
    Connectivity* triToNode = new Connectivity(&fluidDom->faces);

    // if we wish to create overlap
    if (noOverlap==0) {

      // used to create triToTri
      Connectivity* nodeToTri=triToNode->reverse();
      
      // A triToTri connectivity is constructed in order to add
      // layers of overlap at the interface.  If we use we construct
      // subToTri as a transcon across trToTri then the new subToTri
      // will include all elements touching elements in the original.  This
      // will add 2 layers of overlap at the boundaries.  We remove then 
      // one of these layers (the layer lieing in the lower order numbered sub).
      // Then if we perform one more transcon across triToTri we add two more
      // layers giving us the desired three layers (of elements) on the
      // interface.  triToTri is first used in smoothing though to add the
      // the required layers of overlap
      Connectivity* triToTri=triToNode->transcon(nodeToTri);

      // here is the smoothing, if a section is entirely overitten 
      // with smoothUp it will disappear when we smoothDown, similarly
      // we smoothDown twice as far and then back up again to remove any
      // small sections of the subdomains
      Connectivity* triToSub;

      triToSub=this->smoothDown(smoothNum,triToTri);
      delete triToSub; // not used

      triToSub=this->smoothUp(2*smoothNum,triToTri);
      delete triToSub; // not used

      triToSub=this->smoothDown(smoothNum,triToTri);
      
      // Intermediate connectivities are constructed in order to construct
      // the master list, the subToNode and created here doesn't have
      // an overlap at the interface.  The master of the shared nodes at the
      // interface is the subdomain with the smallest number.
      subToNode = subToTri->transcon(triToNode);
      nodeToSub = subToNode->reverse();

      master = new int[fluidDom->nodes.size()];
      for(i=0; i<fluidDom->nodes.size(); i++) {
      	master[i] = (*nodeToSub)[i][0];

      	for(j=1;j<nodeToSub->num(i);j++)
	  master[i] = min((*nodeToSub)[i][j],master[i]);
      }

      // elemMaster keeps track of the initial owner of each tri.
      // this is used when we trim off a layer of the first
      // overlap
      int* elemMaster = new int[fluidDom->faces.size()];
      for(i=0;i<fluidDom->faces.size();i++)
        elemMaster[i]=(*triToSub)[i][0];
      
      cx = new int[fluidDom->faces.size()+1];
      connect = new int[(fluidDom->faces.size())*numSub];
      count=0;

      subToTri = subToTri->transcon(triToTri);
      triToSub = subToTri->reverse();
      for(i=0;i<fluidDom->faces.size();i++){
	cx[i]=count;
	if(triToSub->num(i)==1)
	  connect[count++]=(*triToSub)[i][0];
	else
	  for(j=0;j<triToSub->num(i);j++)
	    if((*triToSub)[i][j]<=elemMaster[i])
	      connect[count++]=(*triToSub)[i][j];
      }
      cx[fluidDom->faces.size()]=count;

      // Delete old connectivities before recreating them
      delete triToSub;
      delete subToTri;

      triToSub = new Connectivity(fluidDom->faces.size(),cx,connect);
      subToTri = triToSub->reverse();

      // At this point all elements containing nodes on the original
      // interface that did lie in the higher number subdomain are
      // now shared by both subdomains.  

      // Build new subToTri connectivity
      // (also keep track of old one  and delete it)
      Connectivity *subToTri_old = subToTri;
      subToTri = subToTri->transcon(triToTri);
      delete subToTri_old;

      // Here we add the final two layers of overlap and go on to define
      // subToSub, subToNode, and nodeToSub in terms of the new overlapped
      // subdomains.

      // Delete old connectivities before recreating them
      delete subToNode;
      delete nodeToSub;

      subToNode = subToTri->transcon(triToNode);
      nodeToSub = subToNode->reverse();

      // Delete local connectivities and arrays
      delete nodeToTri;
      delete triToTri;
      delete triToSub;
      delete [] elemMaster;

    } else {

      // triToNode and subToTri are used to construct subToNode
      subToNode = subToTri->transcon(triToNode);

      // which is then reversed to get nodeToSub
      nodeToSub = subToNode->reverse();

    }

    // subToSub is created from subToNode and nodeToSub
    subToSub  = subToNode->transcon(nodeToSub);

    delete triToNode;

  } else if(numSub==1) {

    // if there is only one subdomain 
    // then no overlap is needed
    connect=new int[fluidDom->nodes.size()];
    cx=new int[2];

    cx[0]=0;
    cx[1]=fluidDom->nodes.size();

    master = new int[fluidDom->nodes.size()];

    for(i=0; i<fluidDom->nodes.size(); i++) {
      master[i]=0;
      connect[i]=i;
    }

    subToNode = new Connectivity(1,cx,connect);
    nodeToSub = subToNode->reverse();
    subToSub  = subToNode->transcon(nodeToSub);

  }
}

//------------------------------------------------------------------------------

void Decomp2D::print() {
  ios_base::fmtflags oldbase;
  
  int i,j,k,ndom;
  int MGN=0,MGE=0,MGI=0;
  char outFile[128];
  int* sent = new int[fluidDom->nodes.size()];
  int* rec  = new int[fluidDom->nodes.size()];
  int* map  = new int[fluidDom->nodes.size()];
  int* numSent = new int[numSub];
  int* numRec  = new int[numSub];

  // below the flu-* files are created  
  if (noOverlapGlobal==0) {
    ofstream decOut ("match.dec",ios::out);
    decOut<<"Decomposition newDecomp for "<<meshName<<endl;
    decOut<<numSub<<endl;

    for(i=0; i<numSub; i++) {
      decOut<<subToTri->num(i)<<endl;

      for(j=0; j<subToTri->num(i); j++)
	decOut<<(*subToTri)[i][j]+1<<endl;
    }

    decOut.close();
    cerr<<"The file: match.dec has been written."<<endl;
  }

  int runs = 0;

 start: ;
  runs++;

  if(runs>2*numSub){
    cerr<<"Matcher can't work with this decomposition"<<endl;
    cerr<<"DONE";
    exit(1);
  }

  for(i=0; i<numSub; i++) {
    sprintf(outFile, "flu-%.5d", i+1);
    ofstream outFilePtr(outFile,ios::out);

    oldbase = outFilePtr.setf(ios::fixed,ios::floatfield);
    
    // a temporary map for the global addresses of the nodes of each
    // subdomain to there local addresses.
    for(j=0; j<fluidDom->nodes.size(); j++)
      map[j] = -1;
    
    for(j=0; j<subToNode->num(i); j++)
      map[(*subToNode)[i][j]] = j;
    
    // output the sub number
    outFilePtr << i+1 << endl;
    
    // output the number of nodes and trits in each subs on 1 line
    outFilePtr << subToNode->num(i) << ' ';
    outFilePtr << subToTri->num(i) << endl;

    int  empty = 0;
    for(j=0; j<nConnect[i]; j++){
      numSent[j] = 0;
      numRec[j]  = 0;
      ndom = connectedDomain[i][j];

      for(k=0; k<(interfNode[i]->num(j)); k++)
        if (master[(*interfNode[i])[j][k]]==i) {
	  sent[numSent[j]] = (*interfNode[i])[j][k];
          numSent[j]++;
	}

      for(k=0; k<(interfNode[i]->num(j)); k++)
        if (master[(*interfNode[i])[j][k]]==ndom) {
          numRec[j]++;
	  rec[numRec[j]] = (*interfNode[i])[j][k];
	}

      if (numRec[j]==0 && numSent[j]==0)
	empty++;
      else if (numRec[j]==0) {

	for(k=0; k<numSent[j]; k++){
	  int tdom, adom = numSub;

	  for(int l=0; l<nodeToSub->num(sent[0]); l++) {
	    tdom = (*nodeToSub)[sent[k]][l];
	    if(tdom!=i&&tdom!=ndom)
	      adom = min(tdom,adom);
	  }

	  if(adom!=numSub)
	    master[sent[k]]=adom;
	  else
	    cerr<<"Matcher can't work with this decomposition"<<endl;
	}
	
	cerr << "Restarting due to empty receive list" << endl;
	goto start;
	
      } else if (numSent[j]==0) {

        for(k=0; k<numRec[j]; k++) {
          int tdom, adom = numSub;

          for(int l=0; l<nodeToSub->num(rec[j]); l++){
            tdom = (*nodeToSub)[rec[k]][l];

            if (tdom!=i&&tdom!=ndom)
              adom = min(tdom,adom);
          }

          if(adom!=numSub)
            master[rec[k]]=adom;
          else
            cerr<<"Matcher can't work with this decomposition"<<endl;
	  
        }   

	cerr<<"Restarting due to empty send list"<<endl;
        goto start; 
      } 
    }

    // If both send and receive lists are empty then we ignore them and 
    // decrease the amount of interfaces output in flu-* accordingly.
    // One or the other (really xor) is empty then the non empty list
    // is reassigned to lowest numbered sub which is not one
    // of the two in question

    // output the number of connected subdomains
    outFilePtr << (nConnect[i]-empty) << endl;
    
    for (j=0; j<nConnect[i]; j++) {
      ndom = connectedDomain[i][j];
      if (numSent[j]!=0) {
	// Output this connected domains number.
      	outFilePtr << ndom+1 << endl;

	// Garbage!!.
	outFilePtr<<0<<endl;

      	// calc the max number of interf nodes sent on all interf for all subs
	// this is the same as the max number received
	MGI=max(MGI,numSent[j]);

	// output the number and list of sent nodes. 
	outFilePtr << numSent[j] << endl;
	for(k=0; k<(interfNode[i]->num(j)); k++)
	  if (master[(*interfNode[i])[j][k]]==i)
	    outFilePtr << map[(*interfNode[i])[j][k]]+1 << endl;	
      }
    }

    for (j=0; j<nConnect[i]; j++) {
      if (numRec[j]!=0) {
	ndom = connectedDomain[i][j];
	outFilePtr << ndom+1 << endl;	
	outFilePtr << 0 << endl;
	outFilePtr << numRec[j] << endl;

      	// output the number and list of received nodes.
	for (k=0; k<(interfNode[i])->num(j); k++)
	  if (master[(*interfNode[i])[j][k]]==ndom)
	    outFilePtr << map[(*interfNode[i])[j][k]]+1 << endl;
      }
    }

    // calc the max number nodes for all subs
    MGN=max(MGN,subToNode->num(i));

    // output the 2D node coords for all nodes in this sub
    for (j=0; j<subToNode->num(i); j++) {
      outFilePtr<<fluidDom->nodes[(*subToNode)[i][j]][0]<<' ';
      outFilePtr<<fluidDom->nodes[(*subToNode)[i][j]][1]<<endl;
    }
    
    // calc max number of tris for all subs
    MGE=max(MGE,subToTri->num(i));

    // output local node numbers for all tris in this sub
    for (j=0;j<subToTri->num(i);j++) {
      for (k=0; k<3; k++)
        outFilePtr<<map[fluidDom->faces[(*subToTri)[i][j]][k]]+1<<' ';
      outFilePtr<<endl;
    }

    // output the node bound conditions
    for (j=0; j<subToNode->num(i); j++)
      outFilePtr<<fluidDom->getTypeBC((*subToNode)[i][j])<<endl;

    // master list is output, the sub # of the master is 
    // is output rather then the boolean list of the 3D case.
    for (j=0; j<subToNode->num(i); j++)
      outFilePtr<<master[(*subToNode)[i][j]]+1<<endl;

    // output a map of local addresses to global addresses for nodes.
    for (j=0; j<subToNode->num(i); j++)
      outFilePtr<<(*subToNode)[i][j]+1<<endl;
		
    // output a map of glabal to local coords.
    for (j=0; j<fluidDom->nodes.size(); j++)
      outFilePtr<<map[j]+1<<endl;

    outFilePtr.setf(oldbase,ios::fixed);
    cerr<<"The file: "<<outFile<<" has been written"<<endl;
    outFilePtr.close();
  }
  
  // below fluid.glob is created
  ofstream outFilePtr("flu.glob",ios::out);

  // 0's (garbage) for the first three lines, we do not need them
  outFilePtr<<0<<endl;
  outFilePtr<<0<<endl;
  outFilePtr<<0<<endl;

  // num subs
  outFilePtr<<numSub<<endl; 

  // num of nodes in domain
  outFilePtr<<fluidDom->nodes.size()<<endl;

  // num trits in domain
  outFilePtr<<fluidDom->faces.size()<<endl; 

  // max number nodes for all subs
  outFilePtr<<MGN<<endl;
  
  // max number tris for all subs
  outFilePtr<<MGE<<endl;

  // max number of interface nodes on all for all subs
  outFilePtr<<MGI<<endl; 

  outFilePtr.close();
  cerr<<"The file: flu.glob has been written."<<endl;

  delete [] numSent;
  delete [] numRec;
  delete [] sent;
  delete [] rec;
  delete [] map;

}
