#ifndef _AUX_H_
#define _AUX_H_

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <cstring>
#include <cmath>
#include <iostream>
using std::ifstream;
using std::ofstream;
using std::ios;
using std::cerr;
using std::cout;
using std::endl;
#include "ResizeArray.h"
#include "Point3D.h"
#include "ElementClass.h"
#include "tools.h"
#include "EFrameData.h"
using namespace std;
/*
struct OutputInfo {
   enum { Displacement, Temperature, StressXX,    StressYY,    StressZZ,
          StressXY,     StressYZ,    StressXZ,    StrainXX,    StrainYY,
          StrainZZ,     StrainXY,    StrainYZ,    StrainXZ,    HeatFlXX,
          HeatFlXY,     HeatFlXZ,    GrdTempX,    GrdTempY,    GrdTempZ,
          StressVM,     InXForce,    InYForce,    InZForce,    AXMoment,
          AYMoment,     AZMoment,    Energies,    AeroForce,   EigenPair,
          StrainVM,     Helmholtz,   Velocity};
   int   interval[32];
   char* filename[32];
   FILE* filptr[32];
   int   averageFlg[32];
};
*/
struct OutputInfo {
   enum Type
        { Displacement, Temperature, StressXX,    StressYY,    StressZZ,
          StressXY,     StressYZ,    StressXZ,    StrainXX,    StrainYY,
          StrainZZ,     StrainXY,    StrainYZ,    StrainXZ,    HeatFlXX,
          HeatFlXY,     HeatFlXZ,    GrdTempX,    GrdTempY,    GrdTempZ,
          StressVM,
          StressPR1,    StressPR2,   StressPR3,
          StrainPR1,    StrainPR2,   StrainPR3,
          InXForce,     InYForce,    InZForce,    AXMoment,
          AYMoment,     AZMoment,    Energies,    AeroForce,   EigenPair,
          StrainVM,     Helmholtz,   Disp6DOF,    AeroXForce,  AeroYForce,
          AeroZForce,   AeroXMom,    AeroYMom,    AeroZMom,    Velocity,
          Acceleration, YModulus,    MDensity,    Thicknes,    ShapeAtt,
          ShapeStc,     Composit,       DispX,       DispY,       DispZ,
          RotX,         RotY,            RotZ,     DispMod,      RotMod,
          TotMod,       Rigid,     ElemToNode,  NodeToElem,  NodeToNode,
          AeroFlux,     HeatFlX,      GrdTemp,   Velocity6,      Accel6,
          ModeAlpha,    ModeError,  Reactions,    ModalDsp,    ModalExF,
          ContactPressure, NodalMassMat, NodalStifMat, Farfield, EigenPressure,
          FreqRespModes, HelmholtzModes, StressPR1Direc, StressPR2Direc,
          StressPR3Direc, StrainPR1Direc, StrainPR2Direc, StrainPR3Direc };

   Type  type;
   int   interval;
   char* filename;
   FILE* filptr;
   int   averageFlg;
   int   surface;
   int   width;
   int   precision;
   int   nodeNumber;    // To output just one node's information to a file.
   int   dim;           // dimension of output data
   int   dataType;      // 1 for nodal, 2 for elemental
   double ylayer,zlayer;

   enum { realimag, modulusphase, animate };
   int complexouttype;
   int ncomplexout;
};


struct FileNames {

  char *prefix;
  char *fluid;	// fluid mesh file
  char *structure;	// struct mesh file
  char *decF;  	// fluid decomp file
  char *decS;	// struct decomp file
  char *match;
  char *outFile;
  char *bcFile; // fluid control file
  char *control;// struct control file
  char *matchedNodes; // PJSA: input file containing node/node pairs that user wants to match together
  char *matchedElements; // PJSA: input file containing node/element pairs that user wants to match together

public:

  FileNames()
  {
    prefix = NULL;
    fluid = NULL;
    structure = NULL;
    decF = NULL;
    decS = NULL;
    match = NULL;
    outFile = NULL;
    bcFile = NULL;
    control = NULL;
    matchedNodes = NULL; 
    matchedElements = NULL;
  }
  ~FileNames() {}

};


typedef struct {
   int num;
   double xyz[3];
   int cp;
} NumedNode;

typedef struct {
   int num;
   int nd[32];
} NumList;

struct NumedElem {
  int num;
  ElementClass *elem;
};

struct NodalFrameData {
  int id;
  double o[3];
  double d[9];
  int type;
};

struct interCoords{
	int success;
	int elNum;
	double xi;
	double eta;
	Point3D point;
	int domNum;
};

class Domain {
private:
	ResizeArray<Point3D *> allNodes;
	ResizeArray<ElementClass *> allElem;
	int* belongsTo;
	int maxNode;
	int maxElem;
	int numSub;
	int numElem;
        int numNframes;
        ResizeArray<NFrameData> nfd;
public:
	Domain();
	void addElem(int, ElementClass *);
	void addNode(int, double[3], int=0);
	void readAux(char* auxFile);
	void getDecomp(char* name);
	int getNumElem(){return numElem;}
	int getNumSub(){return numSub;}
	void matchPoint(Point3D& p, double tol, interCoords& coords, double maxdist);
        void matchPoint(int node, interCoords& coords); // PJSA
        void matchPoint(int element, Point3D& p, double tol, interCoords& coords); // PJSA
	void printAllNodes();
	void printAllElem();
        int setNodalFrame(int, double *, double *, int);
        void transformCoords();
        bool basicPosCoords;
};

extern void buildDomain();
extern Domain* domain;
extern ElementClass* buildElem(int type,int size, int* nodes); 

int yylex(void);

void yyerror(const char*);

struct DoubleList {
   int nval;
   double v[32];
};

#endif
