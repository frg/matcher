#ifndef	_FACE_H_
#define	_FACE_H_
#include<cstdlib>
#include<cassert>
#include<iomanip>
#include<iostream>
using std::ostream;
using std::istream;

#include "BlockAlloc.h"

//--------------------------------------------------------------------

class Face {
public:
  enum Type {QUAD=2, TRIA=3};
  static const int MaxNumNd = 4;

private:
  int type_bc;

protected:

  virtual int* nodeNum() = 0;
  virtual int& nodeNum(int i) = 0;

public:
  virtual int getType() = 0;

  // Number of nodes
  virtual int numNodes() = 0;

  // Operator Face[i]: get reference to node i
  int &operator[](int i) { return nodeNum(i); }  

  // Operator *(Face): get pointer to node list
  operator int *() { return nodeNum(); }

  // Fills the nd array with the list of nodes
  // and returns pointer to list of nodes
  int* nodes(int* d = NULL) {  
    if (d)
      for (int i=0; i<numNodes(); i++) d[i] = nodeNum(i); 
    return nodeNum();
  }

  friend ostream& operator<<(ostream& os, Face& source) {
    int i;
    for (i=0; i<source.numNodes()-1; i++)
      os<<source[i]+1<<' ';
    return os<<source[i]+1;
  }

  friend istream& 
  operator>>(istream& is, Face& source){
    for (int i=0; i<source.numNodes(); i++) {
      is>>source[i];
      source[i]--;
    }
    return is;
  }

  void setTypeBC(int t) {type_bc = t;}
  int  getTypeBC() {return type_bc;}
  
};

//--------------------------------------------------------------------

class FaceQuad : public Face {
private:

  int node[4];

protected:
  int* nodeNum() { return node; }
  int& nodeNum(int i) { return node[i]; }

public:
  int numNodes() { return 4; }
  int getType()  { return QUAD; }

  FaceQuad() {
    for (int i=0; i<numNodes(); i++) 
      nodeNum(i) = -1;
  }

  FaceQuad(int *nn) {
    for (int i=0; i<numNodes(); i++)
      nodeNum(i) = nn[i];
  }

  FaceQuad(int _n1, int _n2, int _n3, int _n4) {
    node[0] = _n1; 
    node[1] = _n2;
    node[2] = _n3;
    node[3] = _n4;
  }

};  

//--------------------------------------------------------------------

class FaceTria : public Face {
private:

  int node[3];

protected:
  int* nodeNum() { return node; }
  int& nodeNum(int i) { return node[i]; }

public:
  int numNodes() { return 3; }
  int getType()  { return TRIA; }

  FaceTria() {
    for (int i=0; i<numNodes(); i++) 
      nodeNum(i) = -1;
  }

  FaceTria(int *nn) {
    for (int i=0; i<numNodes(); i++)
      nodeNum(i) = nn[i];
  }

  FaceTria(int _n1, int _n2, int _n3) {
    node[0] = _n1; 
    node[1] = _n2;
    node[2] = _n3; 
  }

};  

//--------------------------------------------------------------------

class FaceSet {

  int numFaces;

  Face **faces;
  int    size_max;

  BlockAlloc memFaces;
  
public:

  FaceSet() {
    numFaces = 0;
    size_max = 0;
    faces = NULL;
  }

  ~FaceSet() {
    numFaces = 0;
    size_max  = 0;
    delete [] faces;
  }

  Face &operator[](int i) { return *faces[i]; }

  int size() {return numFaces;}
  
  void addFace(int i, Face *face) { 
    faces[i] = face; 
  }

  int resize(int nf) {
    
    if (nf>size_max) {
      // Only reallocate memory for resized array 
      // if new number of faces is larger 
      // than current size_max
      Face **faces_new = new Face*[nf];
    
      // Copy old array to new one
      for (int i=0; i<size_max; i++)
	faces_new[i] = faces[i];

      // Delete old array
      delete [] faces;

      // Set fields to new values
      faces = faces_new;
      size_max  = nf;
    }
    
    // New number of faces
    return numFaces = nf;
  }

  BlockAlloc &getMem() {return memFaces;}
  
};

//--------------------------------------------------------------------

#endif
