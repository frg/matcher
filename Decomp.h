#ifndef _DECOMP_
#define _DECOMP_
#include "Point3D.h"
#include "Elem.h"
#include "Face.h"
#include "ResizeArray.h"
#include "Connectivity.h"
#include "FluidDomain.h"
#include "BlockAlloc.h"
#include "aux.h"

extern int dimension;

class Decomp{

protected:

  // Pointer to a struct containing all information
  // inputted from the fluid domain input file
  FluidDomain* fluidDom; 

public:

  virtual void getFluidDomain(char* name)=0;
  virtual void getDecomp(char* name)=0;
  virtual void getDomainConnect()=0;
  virtual void makeSubD(int noOverlap)=0;
  virtual int getNumSub()=0;
  virtual Connectivity* getSubToNode()=0;
  virtual FluidDomain* getDomain()=0;
  virtual void print()=0;

};

#endif
